#include<stdio.h>
#include<easyx.h>
#include <graphics.h>
#include <windows.h>
#include <conio.h>
#include <mmsystem.h> //导入声音头文件库

#pragma comment(lib,"winmm.lib")//导入声音的链接库

using namespace std;
IMAGE  background1,background2, ball1,ball2, smallball2, ufo1,ufo2, barrier1, barrier2, barrier3,  barriertop, barrierunder,ccandy;//定义一个图片名.
/*
background1第一个游戏背景
background2第二个游戏背景
ball1第一个游戏的球
ball2第二个游戏的球
smallball2第二个游戏的球变小后的小球
ufo1第一个游戏背景
ufo2第二个游戏背景
barriertop第一个游戏的上障碍物
barrierunder第二个游戏的下障碍物
barrier1第二个游戏的一号障碍物
barrier2第二个游戏的二号障碍物
barrier3第二个游戏的三号障碍物
ccandyy糖果的图片
*/
int Length = 1300, Width = 850;//设置界面长和宽
int timescore(0), nowscore(0), highscore1(0), highscore2(0), hp(3),  speed(10);
/*
timescore记录时间
nowscore当前分数
highscore1第一个游戏的最高分记录
highscore2第二个游戏的最高分记录
hp玩家的血条
speed移动速度
*/
int radius = 40,  radiuss=40,smallradius=10,size=0;
/*
radius当前小球半径
radiuss原始小球半径
smallradius第二个游戏变小的小球半径
size判断当前是大球还是小球
*/
char c[10];//记录分数字符
int underfloor = 655,time=0;//地线和时间
int x = 100, y = underfloor - radius;//小球的x,y坐标
int jumpspeed = 0, id(0), tim(0), v = 10, hphs(-1), space(0), barrier = 60;//barrier障碍物
/*
jumpspeed跳跃速度
id为障碍物的id
tim计时间
hphs用来判断是否可以扣血
speed=玩家的速度
*/
int obstacle[50], obstaclex[50] = { -999 }, obstacley[50] = { -999 }, obstaclenum[50];//设置障碍的变量数组
bool jump = true, secondjump = true, hph = true, ob = true, shh = true;
//操作介绍：a左，d右，w上。1,2,3,4调节跳跃高度
// 资源结构体
enum myMenu
{
	Author, // 作者(关于我们)
	Rules,  // 游戏规则
	Start,  // 开始游戏，进入模式选择
	Easy,   // 简单模式
	Difficult, // 困难模式
	Home    // 回到主目录
};
enum myMenu menuState = Home;  // 一开始在主页
int which = -1;                 // 现在是哪个画面了
struct  Resource
{
	// 定义一个图片数组
	IMAGE img_start[5];
	IMAGE img_homePage[3];  // home菜单跳转到的页面
}res;  // 定义一个全局变量
void res_init();

void playMusic()
{
	mciSendString("open music/music.mp3", NULL, NULL, NULL);
	mciSendString("play music/music.mp3 repeat", NULL, NULL, NULL);
}
//drawAlpha用来引入png格式的图片
void drawAlpha(IMAGE* dstimg, int x, int y, IMAGE* srcimg)//x是x轴，y是y轴
{
	if (dstimg == NULL) return;
	DWORD* dst = GetImageBuffer(dstimg);
	DWORD* src = GetImageBuffer(srcimg);
	// 变量初始化
	int src_width = srcimg->getwidth();
	int src_height = srcimg->getheight();
	int dst_width = dstimg->getwidth();
	int dst_height = dstimg->getheight();
	//实现透明贴图 可优化 
	for (int iy = 0; iy < src_height; iy++)
	{
		for (int ix = 0; ix < src_width; ix++)
		{
			int srcX = ix + iy * src_width;
			int sa = ((src[srcX] & 0xff000000) >> 24);
			int sr = ((src[srcX] & 0xff0000) >> 16);
			int sg = ((src[srcX] & 0xff00) >> 8);
			int sb = src[srcX] & 0xff;
			if (x + ix >= 0 && x + ix < dst_width && y + iy >= 0 && y + iy < dst_height)
			{
				int dstX = (x + ix) + (y + iy) * dst_width; int dr = ((dst[dstX] & 0xff0000) >> 16);
				int dg = ((dst[dstX] & 0xff00) >> 8);
				int db = dst[dstX] & 0xff;
				dst[dstX] = ((sr * sa / 255 + dr * (255 - sa) / 255) << 16) | ((sg * sa / 255 + dg * (255 - sa) / 255) << 8) | (sb * sa / 255 + db * (255 - sa) / 255);
			}
		}
	}
}
bool iscandy = 0;//判断是否有糖果
int candy=200;//判断糖果出现时间
int candyx = Length / 2, candyy = Width / 2;//糖果的x,y坐标
//Candy1判断第一个游戏是否要更新糖果位置和判断玩家是否吃到糖果来加血
void Candy1()
{
	if (iscandy==1||candy >= 200)//是否要更新糖果位置
	{
		iscandy = 0;
		candy = 0;
		candyx = rand() % (Length-20)+20;
		candyy = rand() % underfloor;
		printf("%d %d\n", candyx, candyy);
	}
	if (x + radius >= candyx - 50 && x - radius <= candyx + 50 && y + radius >= candyy - 50 && y - radius <= candyy + 50)
	//判断玩家是否吃到糖果来加血
	{
		iscandy = 1;
		candy = 0;
		hp++;
		hp = min(hp, 3);
	}
}
//Candy1判断第二个游戏是否要更新糖果位置和判断玩家是否吃到糖果来加血
void Candy2()
{
	if (iscandy == 1 || candy >= 200)//是否要更新糖果位置
	{
		iscandy = 0;
		candy = 0;
		candyx = rand() % (Length - 20) + 20;
		candyy = rand() % underfloor;
		if(candyy < 200)
		{
			candyy +=250;
		}
		printf("%d %d\n", candyx, candyy);
	}
	if (x + radius >= candyx - 50 && x - radius <= candyx + 50 && y + radius >= candyy - 50 && y - radius <= candyy + 50)
		//判断玩家是否吃到糖果来加血
	{
		iscandy = 1;
		candy = 0;
		hp++;
		hp = min(hp, 3);
	}
}
//判断第二个游戏小球的左右上下移动
void PlayerMove2()
{
	if (y >= underfloor - radius) {//是否可以跳
		jump = true;
		jumpspeed = 0;
	}
	if (y < underfloor - radius) {
		jump = false;//已经在跳了就不能再跳
		jumpspeed += 1 * (radius / 10);
	}
	if (GetKeyState(87) < 0) {//跳
		if (jump == true)
		{
			jump = false;
			jumpspeed = -v;
		}

	}
	if (GetKeyState(65) < 0 && x > 0/*&&left==true*/)x -= speed;//左
	if (GetKeyState(68) < 0 && x < Length - 100)x += speed;//右
	if (GetKeyState(83) < 0 )//小球缩小
	{
		radius = smallradius;
		size = 1;
		time = 0;
		//y = underfloor - radius;
	}
	if (y > underfloor - radius)y = underfloor - radius;//位置归位
	y += jumpspeed;
	x -= 2;
	//v = 30;
}
//第二个游戏的障碍物出现
void ObstacleAppear2() {
	if (tim > space) {//出现障碍（我根据id是奇数的障碍为上面的障碍，id是偶数的为下面的障碍）
		tim = 0;//重置
		if (id >= 50)id = 0;//障碍记号循环
		obstaclex[id] = Length;//奇
		obstacley[id] = underfloor - rand() % (50 * (radius / 10));
		obstaclenum[id] = rand() % 3;
		id++;
	}
}
//第二个游戏的分数的显示
void Scores2()
{
	settextcolor(BLUE);
	settextstyle(15, 0, "宋体");
	outtextxy(10, 10, "当前分数："); //分数显示	
	itoa(nowscore, c, 10);
	outtextxy(80, 10, c);
	outtextxy(10, 30, "最高分数：");
	itoa(highscore2, c, 10);
	outtextxy(80, 30, c);

	outtextxy(10, 60, "变小时间为 1 0，当前变小时间：");
	if(time<10)itoa(time, c, 10);
	else itoa(0, c, 10);
	outtextxy(250, 60, c);
	//outtextxy(10, 50, "按下Esc键暂停游戏");
}
//第二个游戏画面的绘画
void PicDraw2() {//画面绘画
	drawAlpha(&ufo2, 0, 0, &background2);
	//根据size判断当前是打球还是小球
	if(size==0)drawAlpha(&ufo2, x - radius, y - radius, &ball2);
	else drawAlpha(&ufo2, x - radius, y - radius, &smallball2);
	if(iscandy==0)drawAlpha(&ufo2, candyx, candyy, &ccandy);
	Scores2();
	setfillcolor(BLACK);//地面描绘
	solidrectangle(0, underfloor, Length, underfloor + 5);
	setfillcolor(RED);//血量描绘
	solidrectangle(x + 5 - radius, y - 10 - radius, (x + radius - 5) - ((2 * radius - 10) / 3 * (3 - hp)), y - 5 - radius);
}
//第二个游戏的障碍物往左移
void Obstaclesmove2()//障碍物往左移动
{
	for (int i = 0; i < 50; i++) {
		obstaclex[i] -= 7;//障碍移动
		//根据obstaclenum[i]来判断出现不同的虫子
		{
			if (obstaclenum[i] == 0)drawAlpha(&ufo2, obstaclex[i], obstacley[i] - barrier, &barrier1);
			else if (obstaclenum[i] == 1)drawAlpha(&ufo2, obstaclex[i], obstacley[i] - barrier, &barrier2);
			else drawAlpha(&ufo2, obstaclex[i], obstacley[i] - barrier, &barrier3);
		}
		//判断是否碰到障碍物扣血
		if ((x + radius) > obstaclex[i] && (x - radius) < obstaclex[i] + barrier && (y + radius) > obstacley[i] - barrier && (y - radius) < obstacley[i] && hph == true) {//扣血机制
			hp--;
			PicDraw2();
			hph = false;//判断是否可以扣血状态
			hphs = timescore + barrier/2/2;//用来保证有一段时间碰过这个障碍物不会重复扣血
			printf("%d %d\n", hphs, timescore);
		}


	}
}
//第二个游戏的主要函数
void game2() {
	loadimage(&ufo2, "imgs/start/beijing.png", Length, Width, 1);//从图片文件获取图像
	loadimage(&background2, "imgs/start/beijing.png", Length, Width, 1);//从图片文件获取图像
	loadimage(&ball2, "imgs/start/ball.png", 2 * radius, 2 * radius, 1);//从图片文件获取图像
	loadimage(&smallball2, "imgs/start/ball.png", smallradius*2, smallradius*2, 1);//从图片文件获取图像
	loadimage(&barrier1, "imgs/start/1.png", barrier, barrier, 1);//从图片文件获取图像
	loadimage(&barrier2, "imgs/start/2.png", barrier, barrier, 1);//从图片文件获取图像
	loadimage(&barrier3, "imgs/start/3.png", barrier, barrier, 1);//从图片文件获取图像
	putimage(0, 0, &background2);//绘制图像到屏幕，图片左上角坐标为(0,0)
	BeginBatchDraw();//开始绘图
	//初始化变量
	{
		timescore = nowscore = hphs = tim = time = 0; hp = 3; radius = radiuss;
		space = 60;//障碍物的间距
		v = radius; x = 100, y = underfloor - radius; hph = true;
		iscandy = 0; candy = 200; candyx = Length / 2; candyy = Width / 2;
		memset(obstaclex, 0, sizeof(obstaclex));
		memset(obstacley, 0, sizeof(obstacley));
		memset(obstaclenum, 0, sizeof(obstaclenum));id = 0;
	}
	while (1) {
		Candy2();//调用糖果函数
		Sleep(50);//每次刷新时间间隔
		/*if (ob == true)*/tim++,time++,nowscore++,candy++;//定时器
		timescore++;//当前时间分数
		if (time >= 10)radius = radiuss,size=0;//小球模式保持10秒后自动变为大球
		setfillcolor(WHITE);
		putimage(0, 0, &background2);//绘制图像到屏幕，图片左上角坐标为(0,0)
		setbkmode(TRANSPARENT);//设置背景透明
		if (hphs == timescore)hph = true;//间隔时间达到，则开始可触发扣血
		space = 30;//障碍物的间距
		putimage(0, 0, &ufo2);
		PlayerMove2();//判断玩家上下左右
		PicDraw2();//画图
		if (hp <= 0) {//死亡判定
			highscore1 = max(highscore1, nowscore);
			return;
		}
		ObstacleAppear2();//障碍物出现
		Obstaclesmove2();//障碍物移动
		putimage(0, 0, &ufo2);
		PicDraw2();
		FlushBatchDraw();//结束绘图
	}//while
}


//判断第一个游戏小球的左右上下移动
void PlayerMove1()//判断小球的左右上下移动
{
	if (y >= underfloor - radius) {//是否可以跳
		jump = true;
		jumpspeed = 0;
	}
	if (y < underfloor - radius) {
		jump = true;
		jumpspeed = 1;
	}
	if (GetKeyState(87) < 0 && jump == true) {//跳
		jump = false;
		jumpspeed = -v;
	}
	if (GetKeyState(65) < 0 && x > 0/*&&left==true*/)x -= speed;//左
	if (GetKeyState(68) < 0 && x < Length)x += speed;//右
	if (GetKeyState(83) < 0 &&  y <= underfloor-radius)y += 5;//向下移动
	if (y > underfloor - radius)y = underfloor - radius;//位置归位
	y += jumpspeed;
	v = 10;
}
//第一个游戏的障碍物出现
void ObstacleAppear1() {//障碍物出现
	if (tim > space) {//出现障碍（我根据id是奇数的障碍为上面的障碍，id是偶数的为下面的障碍）
		tim = 0;//重置
		if (id >= 50)id = 0;//障碍记号循环
		obstaclex[id] = Length;//奇
		obstacley[id] = rand() % (Length-underfloor-radius*3);
		id++;
		obstaclex[id] = Length;//偶
		obstacley[id] = obstacley[id - 1] + radius+radius*2;
		id++;
	}
}
//第一个游戏的障碍物往左移
void Obstaclesmove1()//障碍物往左移动
{
	for (int i = 0; i < 50; i++) {
		obstaclex[i] -= 5;//障碍移动
		if (i % 2 == 0) {//奇上
			drawAlpha(&ufo1, obstaclex[i],0-Width+ obstacley[i], &barriertop);
			//solidrectangle(obstaclex[i], 0, obstaclex[i] + 10, obstacley[i]);//障碍描绘
			if ((x + radius) > obstaclex[i] && (x - radius) < obstaclex[i] + barrier && (y - radius) < obstacley[i] && hph == true) {//扣血机制
				hp--;
				hph = false;
				//hphs = timescore + 20;
				hphs = timescore + barrier / 2 / 2;
			}//if		

		}
		if (i % 2 == 1) {//偶下
			drawAlpha(&ufo1, obstaclex[i], obstacley[i], &barrierunder);
			//solidrectangle(obstaclex[i], obstacley[i], obstaclex[i] + 10, 410);//障碍描绘
			if ((x + radius) > obstaclex[i] && (x - radius) < obstaclex[i] + barrier && (y + radius) > obstacley[i] && hph == true) {//扣血机制
				hp--;
				hph = false;
				//hphs = timescore + 20;
				hphs = timescore + barrier / 2 / 2;

			}//if
			//if (vis[id] == false && x > obstaclex[i] + 10 && abs(obstacley[i] - obstacley[(i - 2 + 50) % 50]) >= 30)nowscore += (obstacley[i] - obstacley[(i - 1 + 50) % 50]) * 3, vis[id] = true;
			//else if (vis[id] == false && x > obstaclex[i] + 10 && abs(obstacley[i] - obstacley[(i - 2 + 50) % 50]) >= 20)nowscore += (obstacley[i] - obstacley[(i - 1 + 50) % 50]) * 2, vis[id] = true;
			//else if (vis[id] == false && x > obstaclex[i] + 10)nowscore += obstacley[i] - obstacley[(i - 1 + 50) % 50], vis[id] = true;

		}//if
	}
}
//第一个游戏的分数的显示
void Scores1()//显示分数
{
	settextcolor(BLUE);
	settextstyle(15, 0, "宋体");
	outtextxy(10, 10, "当前分数："); //分数显示
	itoa(nowscore, c, 10);
	outtextxy(80, 10, c);
	outtextxy(10, 30, "最高分数：");
	itoa(highscore1, c, 10);
	outtextxy(80, 30, c);
	//outtextxy(10, 50, "按下Esc键暂停游戏");
}
//第一个游戏画面的绘画
void PicDraw1() {//画面绘画
	drawAlpha(&ufo1, 0, 0, &background1);
	drawAlpha(&ufo1, x - radius, y - radius, &ball1);
	if(iscandy==0)drawAlpha(&ufo1, candyx,candyy, &ccandy);//判断是否有糖果并出现糖果
	Scores1();
	setfillcolor(BLACK);//地面描绘
	solidrectangle(0, underfloor, Length, underfloor + 5);
	setfillcolor(RED);//血量描绘
	solidrectangle(x + 5 - radius, y - 10 - radius, (x + radius - 5) - ((2 * radius - 10) / 3 * (3 - hp)), y - 5 - radius);
}
void game1()
{
	loadimage(&ufo1, "pic/beijing1.png", Length, Width, 1);//从图片文件获取图像
	loadimage(&background1, "pic/beijing1.png", Length, Width, 1);//从图片文件获取图像
	loadimage(&ball1, "pic/ball1.png", 2 * radius, 2 * radius, 1);//从图片文件获取图像
	loadimage(&barriertop, "pic/1.png", barrier, Width, 1);//从图片文件获取图像
	loadimage(&barrierunder, "pic/2.png", barrier, Width, 1);//从图片文件获取图像
	
	putimage(0, 0, &background1);//绘制图像到屏幕，图片左上角坐标为(0,0)
	BeginBatchDraw();//开始绘图
	PicDraw1();
	//初始化变量
	{
		space = 60;//障碍物的间距
		timescore = nowscore = hphs = tim = 0; hph = true;
		tim = space + 1; radius = radiuss; hp = 3; id = 0;
		iscandy = 0; candy = 200; candyx = Length / 2; candyy = Width / 2;
		memset(obstaclex, 0, sizeof(obstaclex));
		memset(obstacley, 0, sizeof(obstacley));
		memset(obstaclenum, 0, sizeof(obstaclenum));
		x = 100, y = underfloor - radius;
	}
	while (1) {
		Candy1(); // 调用糖果函数
		Sleep(50);//每次刷新时间间隔
		/*if (ob == true)*/tim++, nowscore++;//定时器
		timescore++;//当前时间分数
		setfillcolor(WHITE);
		setbkmode(TRANSPARENT);//设置背景透明
		//printf("%d %d %d\n", hphs, timescore,hph);
		if (hphs == timescore)hph = true;//间隔时间达到，则开始可触发扣血
		//if (tim % 200 == 0&&space>40)space--;
		putimage(0, 0, &ufo1); //绘制图像到屏幕，图片左上角坐标为(0, 0)
		PlayerMove1();//判断玩家上下左右
		PicDraw1();//画图
		if (hp <= 0) {//死亡判定
			highscore1 = max(highscore1, nowscore);
			return;
		}
		ObstacleAppear1();//障碍物出现
		Obstaclesmove1();//障碍物移动
		putimage(0, 0, &ufo1);
		PicDraw1();
		FlushBatchDraw();
	}
}

void DIE()
{

	HWND hnd = GetHWnd();
	int isok = MessageBox(hnd, "你好，太子！", "提示", MB_OKCANCEL);
	if (isok == IDOK)
	{
		printf("你点击了OK\n");
	}
	else if (IDCANCEL == isok)
	{
		printf("你点击了取消\n");
	}
}
//初始化资源
void res_init(struct Resource* res)
{
	for (int i = 0; i < 1; i++)
	{
		char path[50] = { 0 };
		sprintf_s(path, "imgs/0%d.jpg", i + 1);
		loadimage(res->img_start + i, path, getwidth(), getheight());
	}

	// 加载home子页
	// 作者
	loadimage(res->img_homePage + 0, "imgs/pageImgs/author.jpg");
	// 游戏规则
	loadimage(res->img_homePage + 1, "imgs/pageImgs/rules.jpg");
	// 开始游戏进去模式选择
	loadimage(res->img_homePage + 2, "imgs/pageImgs/level.jpg");


}

// 判断鼠标是否位于矩形区域
bool isInRect(ExMessage* msg, int x, int y, int w, int h)
{
	if (msg->x > x && msg->x < x + w && msg->y > y && msg->y < y + h)
	{
		return true;
	}
	return false;
}
// 菜单选项
// 启动场景
void startupScene(ExMessage* msg)
{
	// 鼠标左键点击切换画面
	
	if (msg->message == WM_LBUTTONDOWN) 
	{
			// 切换页面
		if (which==-1)//在主页面时
		{
			// 1.点击[关于我们]按钮
			//solidrectangle(0, 200, 210, 150);
			if (isInRect(msg, 520, 200, 210, 150))
			{
				menuState = Author;
				which = 0;
			}
			// 2. 点击[游戏规则]按钮
			else if (isInRect(msg, 520, 300, 210, 150))
			{
				menuState = Rules;
				which = 1;
			}
			// 3. 点击[开始游戏]按钮
			else if (isInRect(msg, 520, 420, 190, 110))
			{
				menuState = Start;
				which = 2;
			}
			// 4.点击[退出游戏]按钮
			else if (isInRect(msg, 530, 500, 190, 110))
			{
				exit(-1);
			}

		}
		else if(which==2) //进入选择简单困难模式页面
		{
			// 1.点击[简单模式]按钮isInRect(msg, 485, 290, 330, 90
			if (isInRect(msg, 485, 290, 330, 90))
			{
				// 简单模式
				game1();
				menuState = Home;
				which = -1;
			}
			
			// 2. 点击[困难模式]按钮
			else if (isInRect(msg, 485, 490, 330, 90))
			{
				// 困难模式
				
				game2();
				menuState = Home;
				which = -1;
			}
			//点击返回上一页按钮
			else if (isInRect(msg, 20, 20, 70, 70))
			{
				menuState = Home;
				which = -1;
			}
		}
		else 
		{
			switch (menuState)
			{
				case Author://[关于我们页面]
					if(isInRect(msg,150,30,80,80)) 
					{
						menuState = Home;
						which = -1;
					}
					break;
				case Rules://[规则]
					if (isInRect(msg, 20, 20, 70, 70))
					{
						menuState = Home;
						which = -1;
					}
					break;
				case Start:
					if (isInRect(msg, 20, 20, 70, 70))
					{
						menuState = Home;
						which = -1;
					}
					break;
			}
		}
		FlushBatchDraw();
		printf("state:%d\n  which:%d\n",menuState,which);
	}

	
	// 绘制图片
	//printf("%d\n",which);
	if (which==-1)//主页面
	{
		putimage(0, 0, res.img_start + 0);
		playMusic();
	}
	else//其他页面
	{
		putimage(0, 0, res.img_homePage + which);
	}
	FlushBatchDraw();
}

int main() {
	// 创建窗口
	initgraph(1280, 850, EX_SHOWCONSOLE);
	
	res_init(&res);
	loadimage(&ccandy, "imgs/start/candy.png", 50, 50, 1);//从图片文件获取图像
	while (true)
	{
		// 处理消息,不断获取鼠标消息，如果有消息返回true，否则返回false
		ExMessage msg;
		while (peekmessage(&msg, EX_MOUSE))
		{
			startupScene(&msg);
		}
	}

	getchar();
	return 0;
}